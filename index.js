var http = require('http');
require("babel/register");

var program = require("./program.js");

var wordList = [];
var searchProgram = program(wordList);

function test(){
	console.log("test");
}

http.createServer(function(req, res) {
    var query = req.url.split("/").join("");
    var matches = searchProgram(query);
    res.writeHead(200, {
        'Content-Type': 'text/json'
    });
    matches.myFunc = test;
    console.log(matches);

    res.end(JSON.stringify(matches));

}).listen(9615);


